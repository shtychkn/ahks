SetTimer, RemoveToolTip, -5000
return

F9:: pause, toggle

F8::

;Mouse over GG mark then press F9 to start
loop
{
	;buy marks
	loop 9
	{
		Send {lbutton down}
		Sleep, 5000
		Send {lbutton up}
		MouseMove, 0, 50, 5, R
		MouseMove, 0, -50, 5, R
	}
;exit and go to dolly screen
	Sleep, 1000
	Send {esc}
	Sleep, 1000
	Send {d}
	Sleep, 1000
	Send {d}
	Sleep, 1000

;move mouse to mark you are wearing to trigger pop-out, use windowspy.ahk in the ahk install folder to find _your_ window positions
	MouseMove, 1390, 774
	
;move mouse to first position inventory mark
	MouseMove, 1501, 774
	
;balete marks
	loop 9
	{
		Send {f down}
		Sleep, 5000
		Send {f up}
		MouseMove, 0, 50, 5, R
		MouseMove, 0, -50, 5, R
	}
	Sleep, 1000
;navigate to Collections
	loop 2
	{
		Send {a}
		Sleep, 1000
	}
;move mouse to Armor tile
	MouseMove, 880, 416
	Send {LButton}
	Sleep, 1000

;navigate to events window, third page
	loop 4
	{
		Send {s}
		Sleep, 1000
	}

	loop 2
	{
		Send {Right}
		Sleep, 1000
	}
	
;move mouse to Guardian Games Dress
	MouseMove, 724, 471
}
RemoveToolTip:
ToolTip
return

F10::
ExitApp
